const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

const db = new Sequelize('passportdb', 'ppuser', 'pppass', {
    dialect: 'mysql'
})

const patient_master = db.define('patient_master', {
    patient_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    presc_date: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    }
})

const response_master = db.define('response_master', {
    response_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    patient_id: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    },
    created_on:{
        type:DataTypes.STRING,
        allowNull:false,
    }
})
const response_details = db.define('response_details', {
    response_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    response_text: {
        type: DataTypes.STRING,
        allowNull: false
    }
})
const email=db.define('email',{
    mail:{
        type:DataTypes.STRING
    }
})
const s=db.define('s',{
    sur:{
        type:DataTypes.INTEGER
    }
})

db.sync().then(() => "Database created")

exports = module.exports = {
    models: {
        db,
        patient_master,s,email, response_details, response_master
    }

}